//-----------------------------------------------------------------------------------*
// Application Name :   Common Utility 
// Object Id        :   PJ-18
// Release          :
// Author           :   Mahesh Magar
// Date             :   12/12/2022
// Description      :   Job Execution Variable Managament
//-----------------------------------------------------------------------------------*
//Descriptions: UI Annotations definition
//-----------------------------------------------------------------------------------*
//Change Log:
//    Date      |   Author      |   Defect/Incident     |   Change Description
//
//-----------------------------------------------------------------------------------*/
using CommonUtilityService as service from '../../srv/commonutility-service';

annotate service.JobExecutionVariables with @(UI : {
    HeaderInfo      : {
        $Type          : 'UI.HeaderInfoType',
        TypeName       : '{i18n>lblHeader}',
        TypeNamePlural : '{i18n>lblHeaderPlural}',
    },
    SelectionFields : [
        jobId,
        varName,
        seqNo
    ],
    LineItem        : [
        {
            $Type : 'UI.DataField',
            Label : '{i18n>lblJobId}',
            Value : jobId,
        },
        {
            $Type : 'UI.DataField',
            Label : '{i18n>lblVarName}',
            Value : varName,
        },
        {
            $Type : 'UI.DataField',
            Label : '{i18n>lblSeqNo}',
            Value : seqNo,
        },
        {
            $Type : 'UI.DataField',
            Label : '{i18n>lblVarType}',
            Value : varType,
        },
        {
            $Type : 'UI.DataField',
            Label : '{i18n>lblDynVarId}',
            Value : dynVarId,
        },
        {
            $Type : 'UI.DataField',
            Label : '{i18n>lblDataType}',
            Value : dataType,
        },
        {
            $Type : 'UI.DataField',
            Label : '{i18n>lblSign}',
            Value : sign,
        },
        {
            $Type : 'UI.DataField',
            Label : '{i18n>lblLowValue}',
            Value : lowValue,
        },
        {
            $Type : 'UI.DataField',
            Label : '{i18n>lblHighValue}',
            Value : highValue,
        }

    ]
});

annotate service.JobExecutionVariables with @(
    UI.FieldGroup #ExecVarInfo : {
        $Type : 'UI.FieldGroupType',
        Data  : [
            {
                $Type : 'UI.DataField',
                Label : '{i18n>lblJobId}',
                Value : jobId,
            },
            {
                $Type : 'UI.DataField',
                Label : '{i18n>lblVarName}',
                Value : varName,
            },
            {
                $Type : 'UI.DataField',
                Label : '{i18n>lblSeqNo}',
                Value : seqNo,
            },
            {
                $Type : 'UI.DataField',
                Label : '{i18n>lblVarType}',
                Value : varType,
            },
            {
                $Type : 'UI.DataField',
                Label : '{i18n>lblDynVarId}',
                Value : dynVarId,
            },
            {
                $Type : 'UI.DataField',
                Label : '{i18n>lblDataType}',
                Value : dataType,
            },
            {
                $Type : 'UI.DataField',
                Label : '{i18n>lblSign}',
                Value : sign,
            },
            {
                $Type : 'UI.DataField',
                Label : '{i18n>lblLowValue}',
                Value : lowValue,
            },
            {
                $Type : 'UI.DataField',
                Label : '{i18n>lblHighValue}',
                Value : highValue,
            },
        ],
    },
    UI.Facets                      : [{
        $Type  : 'UI.ReferenceFacet',
        ID     : 'ExecVarInfo',
        Label  : 'General Information',
        Target : '@UI.FieldGroup#ExecVarInfo',
    }]
);

annotate service.JobExecutionVariables with @(Capabilities : {SearchRestrictions : {
    $Type      : 'Capabilities.SearchRestrictionsType',
    Searchable : false
}});

annotate service.JobExecutionVariables with{
    @Core.Immutable
    jobId;
     
    @Core.Immutable
    varName;

    @Core.Immutable
    seqNo;

     
};
