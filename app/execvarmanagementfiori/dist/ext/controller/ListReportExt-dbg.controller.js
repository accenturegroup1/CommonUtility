/* eslint-disable max-len */
// -----------------------------------------------------------------------------------*
// Application Name :   Common Utility
// Object Id        :   PJ-18
// Release          :
// Author           :   Mahesh Magar
// Date             :   12/12/2022
// Description      :   Job Execution Variable Managament
// -----------------------------------------------------------------------------------*
// Descriptions: UI Controller Extension
// -----------------------------------------------------------------------------------*
// Change Log:
//    Date      |   Author      |   Defect/Incident     |   Change Description
//
// -----------------------------------------------------------------------------------*/
sap.ui.controller('acn.commonutility.ui.execvarmanagementfiori.ext.controller.ListReportExt', {

  onInit: function() {
    const oSmartTable = this.getView().byId('listReport');
    oSmartTable.setEnableAutoColumnWidth(false);
  },

});
