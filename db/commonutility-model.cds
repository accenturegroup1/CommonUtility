//-----------------------------------------------------------------------------------*
// Application Name :   Common Utility Function
// Object Id        :   PJ-17
// Release          :
// Author           :   Mahesh Magar
// Date             :   12/12/2022
// Description      :   Job Execution Variable Managament
//-----------------------------------------------------------------------------------*
//Descriptions: BTP HANA DB Entities
//-----------------------------------------------------------------------------------*
//Change Log:
//    Date      |   Author      |   Defect/Incident     |   Change Description
//
//-----------------------------------------------------------------------------------*/

namespace acn.commonutility.execvariablemanagement;
using {managed} from '@sap/cds/common';
// BTP table for Execution Variable Management Definition Table
@cds.persistence.exists
entity ExecVariableDefinition {

  key jobId     : String(128) not null;                                           // Job Id
  key varName   : String(64)  not null;                                           //Execution variable name
  key seqNo     : Integer     not null;                                           //Sequence Number
      varType   : String(1);                                                      //Variable type
      dynVarId  : String(64);                                                     //Dynamic variable id
      dataType  : String(1);                                                      //Date type of variable
      sign      : String(1);                                                      //Sign
      lowValue  : String(255);                                                    //Low value of range
      highValue : String(255);                                                    //High value of range
      regDate   : Timestamp  @cds.on.insert : $now;                               //Date of creation
      regUser   : String     @cds.on.insert : $user;                              //Created by user Id
      regPgm    : String(40);                                                     //Registering Application Name
      upDate    : Timestamp  @cds.on.insert : $now   @cds.on.update : $now;       //Date of updation
      upUser    : String     @cds.on.insert : $user  @cds.on.update : $user;      //Updatedby user id
      upPgm     : String(40)                                                      //Updating program name
}

// BTP table for Execution Variable Retrieval  Table
entity ExecVariableRetrival {

  key jobId     : String(128) not null;                                           // Job Id
  key varName   : String(64)  not null;                                           //Execution variable name
  key seqNo     : Integer     not null;                                           //Sequence Number
      varType   : String(1);                                                      //Variable type
      dynVarId  : String(64);                                                     //Dynamic variable id
      dataType  : String(1);                                                      //Date type of variable
      sign      : String(1);                                                      //Sign
      lowValue  : String(255);                                                    //Low value of range
      highValue : String(255);                                                    //High value of range
      regDate   : Timestamp  @cds.on.insert : $now;                               //Date of creation
      regUser   : String     @cds.on.insert : $user;                              //Created by user Id
      regPgm    : String(40);                                                     //Registering Application Name
      upDate    : Timestamp  @cds.on.insert : $now   @cds.on.update : $now;       //Date of updation
      upUser    : String     @cds.on.insert : $user  @cds.on.update : $user;      //Updatedby user id
      upPgm     : String(40)                                                      //Updating program name
}
