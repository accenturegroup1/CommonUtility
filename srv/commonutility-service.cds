//-----------------------------------------------------------------------------------*
// Application Name :   Common Utility Function
// Object Id        :   PJ-17
// Release          :
// Author           :   Mahesh Magar
// Date             :   12/12/2022
// Description      :   Job Execution Variable Managament
//-----------------------------------------------------------------------------------*
//Descriptions: Node.js Service layer definition
//-----------------------------------------------------------------------------------*
//Change Log:
//    Date      |   Author      |   Defect/Incident     |   Change Description
//
//-----------------------------------------------------------------------------------*/
using acn.commonutility.execvariablemanagement as db from '../db/commonutility-model';
//import External service data model
using ZAMCM_VARIABLE_MANAGEMENT_SRV as external from './external/ZAMCM_VARIABLE_MANAGEMENT_SRV.cds';

service CommonUtilityService {

    //Service Entity for accessing Execution variable definition

    entity JobExecutionVariables as projection on db.ExecVariableDefinition excluding {
        regDate,
        regPgm,
        regUser,
        upDate,
        upPgm,
        upUser
    };

    //API endpoint to get Execution variable data
    function getExecutionVariables(jobId : String(128), keepVariant : Boolean) returns array of JobExecutionVariables;
    //API endpoint to get delete Execution variable data
    action   deleteExecutionVariable(jobId : String(128))    returns String;
    
    //Change local utc date
    function changeUtcToLocal(sUtcDateTime: DateTime,sOutputDateFormat: String,sDestination: String) returns String;
    function changeLocalToUtc(sLocalDateTime: DateTime,sOutputDateFormat : String) returns String;
   
    //Mass Upload
    action getExcelSheet(sInstanceName:String, sPath:String, sfileName:String, isheetIndex: Integer) returns Integer;
   
   //Convert CSV
   function convertString2JSONArray(sCRLF:String, sSplitType:String, sEnclosingCharacter:String, sFileData:String) returns String;
   function convertString2Date(sDate:String) returns String;
   function convertString2Time(sTime:String) returns String;
   function convertString2Numeric(sNum:String) returns String;

   //validation check
   function validateCheck(oData:String,oValidaters:String,sLocale:String) returns String;
}
