/* eslint-disable brace-style */
/* eslint-disable object-curly-spacing */
/* eslint-disable new-cap */
/* eslint-disable no-unused-vars */
/* eslint-disable max-len */
/* eslint-disable no-prototype-builtins */
// -----------------------------------------------------------------------------------*
// Application Name :   Common Utility Function
// Object Id        :   PJ-17
// Release          :
// Author           :   Mahesh Magar
// Date             :   12/12/2022
// Description      :   Job Execution Variable Managament
// -----------------------------------------------------------------------------------*
// Descriptions: Node.js Service implementation
// -----------------------------------------------------------------------------------*
// Change Log:
//    Date      |   Author      |   Defect/Incident     |   Change Description
//
// -----------------------------------------------------------------------------------*/

const cds = require('@sap/cds');
const log = require('cf-nodejs-logging-support');
// eslint-disable-next-line no-unused-vars
const { parse } = require('csv-parse/sync');
const constants = require('./config/constant');
const btpOperation = require('./operations/btphanaoperations');
const dateConverter = require('./lib/converttolocaldate');
const onPremOperation = require('./operations/onpremoperations');
const CommonLog = require('./lib/commonlogmessage');
const createLogger = require('@sap-cloud-sdk/util').createLogger;
const LOGGER = createLogger('utility-program');
const bulkRegisterUtil = require('./lib/BulkRegisterUtil');
const beancheckutil = require('./lib/BeanCheckUtil');
const callbeancheckutil = require('./lib/callbeancheckutil');
const ConvertCsv = require('./lib/ConvertCsv');
const schema = require('./lib/schema');
const XLSX = require('xlsx');

const utility = new bulkRegisterUtil();
// const {createLogger} = require('@sap-cloud-sdk/util');
const { convertString2Date, convertString2JSONArray } = require('./lib/ConvertCsv');
const ConvertCSV = require('./lib/ConvertCsv');
// const configpro = new TextBundle('./lib/_i18n/conf');

module.exports = async (srv) => {
  /* Event handler for getExecutionVariable */
  srv.on('getExecutionVariables', async (req) => {
    // initialise common log
    const commonLog = new CommonLog(req.locale);

    log.debug(`getExecutionVariables -> start`);
    try {
      let execVarData;
      const { jobId, keepVariant } = req.data;
      let isRetrivedData = true;

      if (jobId && jobId !== '') {
        // get execution variable data from retrieval table
        execVarData = await btpOperation.getExecVarFromRetrival(jobId);

        if (execVarData && Array.isArray(execVarData) && execVarData.length) {
          log.info('processing data from retrieval table');

          isRetrivedData = true;
        } else {
          log.info('processing data from definition table');

          // data is not present in retrieval table, get data from definition table
          execVarData = await btpOperation.getExecVarFromDefinition(jobId);
          isRetrivedData = false;
          // Import data from TVARVC table
          if (execVarData && Array.isArray(execVarData) && execVarData.length) {
            for (const execVar of execVarData) {
              if (execVar.dynVarId && execVar.dynVarId != '') {
                // get Dynamic variable from s/4 tables
                const dynamicVariable = await onPremOperation.
                    getDynamicVariable(req, execVar.dynVarId);
                if (dynamicVariable &&
                  dynamicVariable[0].hasOwnProperty('High') &&
                  dynamicVariable[0].hasOwnProperty('Low')) {
                  execVar.lowValue = dynamicVariable[0].Low;
                  execVar.highValue = dynamicVariable[0].High;
                }
              }
            }
          }
        }

        // If Keep variant is passed save execution variable data in Retrieval table.
        if (keepVariant && !isRetrivedData) {
          const createdEntries = await btpOperation.createExecVarDetails(execVarData);
          if (createdEntries && createdEntries.results) {
            log.info(`${createdEntries.results.length}  entries created for ${jobId}`);
          }
          log.debug(`getExecutionVariables -> end`);
        }

        return execVarData;
      } else {
        return req.error({
          code: constants.httpError400,
          message: commonLog.getLogMessage('errMissingJobId'),
        });
      }
    } catch (error) {
      return req.error({ code: constants.httpError400, message: error.message });
    }
  });

  /* Event handler for deleteExecutionVariable */
  srv.on('deleteExecutionVariable', async (req) => {
    // initialise common log
    log.debug(`deleteExecutionVariable -> start`);
    const commonLog = new CommonLog(req.locale);
    const { jobId } = req.data;
    try {
      if (jobId && jobId !== '') {
        const deletedData = await btpOperation.deleteExecVarFromRetrival(jobId);
        if (deletedData) {
          log.info(`data deleted for jobid ${jobId}`);
          return commonLog.getLogMessage('msgDeleteSuccess', [jobId]);
        }
      } else {
        req.error({
          code: constants.httpError400,
          message: commonLog.getLogMessage('errInvalidJobId'),
        });
      }
    } catch (error) {
      req.error({ code: constants.httpError400, message: error.message });
    }
  });
  // sample for getting data from Odata service connections.
  srv.on('READ', 'TvarcEntityCollection', async (req) => {
    const serviceName = await cds.connect.to(constants.serviceName);
    const result = await serviceName.run(req.query);
    return result;
  });
  // Convert to local date
  // Change UTC to local date
  srv.on('changeUtcToLocal', async (req) => {
    const { sUtcDateTime, sOutputDateFormat, sDestination } = req.data;
    try {
      const dUtcDateTime = new Date(sUtcDateTime);
      const localDate = dateConverter.changeUtcToLocal(dUtcDateTime, sOutputDateFormat, sDestination);
      return localDate;
    }
    // If the value after conversion is null or date conversion fails
    catch (error) {
      LOGGER.error('Failed to get the Local DateTime');

      // return null to caller
      return null;
    }
  });

  // Change local to UTC
  srv.on('changeLocalToUtc', async (req) => {
    const { sLocalDateTime, sOutputDateFormat } = req.data;
    const dLocalDateTime = new Date(sLocalDateTime);
    // Set the retrieved UTC timezone
    try {
      const localDate = dateConverter.changeLocalToUtc(dLocalDateTime, sOutputDateFormat);
      return localDate;
    }
    // When sLocalDateTimeSetUTC is an invalid parameter
    catch (error) {
      LOGGER.error('Failed to get the UTC DateTime');
      // return null to caller
      return null;
    }
  });
  // Common Validation check function
  srv.on('validatecheck', async (req) => {
    const { dataset, schemaset, slocale } = req.data;
    try {
      dataset = JSON.stringify(callbeancheckutil.data.datavalues);
      schemaset = schema.oValidators;
      const msg = beancheckutil.validatecheck(dataset, schemaset, slocale);
      return msg;
    } catch (error) {
      LOGGER.error('Failed to validate check function');
      // return null to caller
      return null;
    }
  });
  // Mass upload excel file retrival function
  srv.on('getExcelSheet', async (req) => {
    const { sInstanceName, sPath, sfileName, isheetIndex } = req.data;
    try {
      const utility = new bulkRegisterUtil();
      const sheetData = utility.getExcelSheet(sInstanceName, sPath, sfileName, isheetIndex);
      return JSON.stringify(sheetData);
    } catch (error) {
      LOGGER.error(error);
      // return null to caller
      return null;
    }
  });

  // Convert CSV File
  srv.on('convertString2JSONArray', async (req) => {
    const { sCRLF, sSplitType, sEnclosingCharacter, sFileData } = req.data;
    try {
      let fileDataResult = [];
      fileDataResult = ConvertCSV.convertString2JSONArray(sCRLF, sSplitType, sEnclosingCharacter, sFileData);
      return fileDataResult;
    } catch (error) {
      LOGGER.error('ERROR');
    }
  });
  // Date string converted to YYYYMMDD format.
  srv.on('convertString2Date', async (req) => {
    const { sDate } = req.data;
    try {
      const sdate = ConvertCSV.convertString2Date(sDate);
      return sdate;
    } catch (error) {
      LOGGER.error('ERROR in converting date string');
    }
  });
  // Time type string converted to HH:MM:SS format
  srv.on('convertString2Time', async (req) => {
    const { sTime } = req.data;
    try {
      const tTime = ConvertCSV.convertString2Time(sTime);
      return tTime;
    } catch (error) {
      LOGGER.error('Error in converting string to time');
    }
  });
  // convertString2Numeric
  srv.on('convertString2Numeric', async (req) => {
    const { sNum } = req.data;
    try {
      const nNum = ConvertCSV.convertString2Numeric(sNum);
      return nNum;
    } catch (error) {
      LOGGER.error('Error in converting string to numeric');
    }
  });
};
