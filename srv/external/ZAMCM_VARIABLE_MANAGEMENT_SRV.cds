/* checksum : a01f86ecfbf8f62772122e3ee009a7bb */
@cds.external : true
@m.IsDefaultEntityContainer : 'true'
@sap.supported.formats : 'atom json xlsx'
service ZAMCM_VARIABLE_MANAGEMENT_SRV {};

@cds.external : true
@cds.persistence.skip : true
@sap.searchable : 'true'
@sap.pageable : 'false'
@sap.content.version : '1'
entity ZAMCM_VARIABLE_MANAGEMENT_SRV.TvarcEntityCollection {
  @sap.unicode : 'false'
  @sap.label : '変数名'
  @sap.creatable : 'false'
  @sap.updatable : 'false'
  @sap.sortable : 'false'
  @sap.filterable : 'false'
  key Name : String(30) not null;
  @sap.unicode : 'false'
  @sap.label : '選択タイプ'
  @sap.creatable : 'false'
  @sap.updatable : 'false'
  @sap.sortable : 'false'
  @sap.filterable : 'false'
  key Type : String(1) not null;
  @sap.unicode : 'false'
  @sap.label : '番号'
  @sap.creatable : 'false'
  @sap.updatable : 'false'
  @sap.sortable : 'false'
  @sap.filterable : 'false'
  key Numb : String(4) not null;
  @sap.unicode : 'false'
  @sap.label : 'INCL/EXCL'
  @sap.creatable : 'false'
  @sap.updatable : 'false'
  @sap.sortable : 'false'
  @sap.filterable : 'false'
  Sign : String(1) not null;
  @sap.unicode : 'false'
  @sap.label : 'オプション'
  @sap.creatable : 'false'
  @sap.updatable : 'false'
  @sap.sortable : 'false'
  @sap.filterable : 'false'
  Opti : String(2) not null;
  @sap.unicode : 'false'
  @sap.label : '項目値'
  @sap.creatable : 'false'
  @sap.updatable : 'false'
  @sap.sortable : 'false'
  @sap.filterable : 'false'
  Low : String(255) not null;
  @sap.unicode : 'false'
  @sap.label : '項目値'
  @sap.creatable : 'false'
  @sap.updatable : 'false'
  @sap.sortable : 'false'
  @sap.filterable : 'false'
  High : String(255) not null;
  @sap.unicode : 'false'
  @sap.label : 'SYST のデータエレメント CHAR01'
  @sap.creatable : 'false'
  @sap.updatable : 'false'
  @sap.sortable : 'false'
  @sap.filterable : 'false'
  ClieIndep : String(1) not null;
};

