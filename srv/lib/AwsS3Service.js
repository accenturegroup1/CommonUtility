const AWS = require('aws-sdk');
const {createLogger} = require('@sap-cloud-sdk/util');
const logger = createLogger('utility-program');
const objectStoreUtil = require('./ObjectStoreUtil');

module.exports = class AwsS3 {
  #objectStoreInfo;
  #s3;
  /**
   * constructor
   * @param {*} objectStoreInfo
   */
  constructor(objectStoreInfo) {
    this.#objectStoreInfo = objectStoreInfo;
    const credentials = new AWS.Credentials(
        objectStoreInfo.access_key_id,
        objectStoreInfo.secret_access_key);

    AWS.config.update({
      region: objectStoreInfo.regin,
    });

    this.#s3 = new AWS.S3({
      credentials: credentials,
      apiVersion: '2006-03-01',
      s3ForcePathStyle: true,
    });
  }

  /**
     * Save the file to S3.
     * @param {*} data Binary data
     * @param {*} path file path
     * @param {*} fileName file name
     * @param {*} contentType data type
     */
  async saveFile(data, path = '', fileName) {
    // Argument checking
    if (!data) throw new Error('file data' + objectStoreUtil.ERR0001);
    // File path generation
    const filePath = this.getFullPath(path, fileName);
    // Existence check
    const exist = await this.isFileExist(filePath);
    if (exist) throw new Error(fileName + objectStoreUtil.ERR0003);
    // param
    const params = {
      Bucket: this.#objectStoreInfo.bucket,
      Key: filePath,
      Body: data,
    };
    // upload
    const promise = new Promise((resolve) => {
      this.#s3.upload(params, function(err, data) {
        if (err) throw new Error(objectStoreUtil.ERR0005 + fileName, err);
        else resolve(data);
      });
    });
    // Existence check
    promise.then(() => {
      return this.isFileExist(filePath);
    }).then((exist) => {
      if (exist) console.log(fileName + objectStoreUtil.INF0001);
      else throw new Error(objectStoreUtil.ERR0005 + fileName);
    }).catch((err) => {
      throw new Error(err);
    });
  }

  /**
   * Get files in the ObjectStore.
   * @param {string} path file path
   * @param {string} fileName file name
   * @return {ArrayBuffer} Binary of file
   */
  async getFile(path = '', fileName) {
    // File path generation
    const filePath = this.getFullPath(path, fileName);
    // Existence check
    const exist = await this.isFileExist(filePath);
    if (!exist) throw new Error(fileName + objectStoreUtil.ERR0004);
    // param
    const params = {
      Bucket: this.#objectStoreInfo.bucket,
      Key: filePath,
    };

    // get file
    let rst = null;
    await new Promise((resolve, reject) => {
      this.#s3.getObject(params, function(err, data) {
        if (err) reject(new Error(objectStoreUtil.ERR0006 + fileName, err));
        else {
          logger.info(fileName + objectStoreUtil.INF0002);
          resolve(data);
        }
      });
    }).then((data) => {
      rst = data.Body;
    }, (err) => {
      throw err;
    }).catch((err) => {
      throw new Error(objectStoreUtil.ERR0006 + fileName, err);
    });
    return rst;
  }

  /**
   * Delete files in the ObjectStore.
   * @param {string} path file path
   * @param {string} fileName file name
   */
  async deleteFile(path = '', fileName) {
    // File path generation
    const filePath = this.getFullPath(path, fileName);
    // Existence check
    const exist = await this.isFileExist(filePath);
    if (!exist) throw new Error(fileName + objectStoreUtil.ERR0004);
    // param
    const params = {
      Bucket: this.#objectStoreInfo.bucket,
      Key: filePath,
    };
    // deleteObject
    const promise = new Promise((resolve) => {
      this.#s3.deleteObject(params, function(err, data) {
        if (err) throw new Error(objectStoreUtil.ERR0007 + fileName, err);
        else resolve(data);
      });
    });
    // Existence check
    promise.then(() => {
      return this.isFileExist(filePath);
    }).then((exist) => {
      if (!exist) console.log(fileName + objectStoreUtil.INF0003);
      else throw new Error(objectStoreUtil.ERR0007 + fileName);
    }).catch((err) => {
      throw new Error(err);
    });
  }

  /**
   * Check if the ObjectStore file exists.
   * @param {string} filepath path + filename
   * @return {boolean} exist: true
   *                   not exist: false
   */
  async isFileExist(filepath) {
    // param
    const params = {
      Bucket: this.#objectStoreInfo.bucket,
    };
    let rst = false;
    // list of keys
    await new Promise((resolve, reject) => {
      this.#s3.listObjects(params, function(err, data) {
        if (err) reject(err);
        else {
          let keys = [];
          if (data.Contents.length > 0) {
            keys = data.Contents.map((item, next) => {
              return item.Key;
            });
          }
          resolve(keys);
        }
      });
    }).then((data) => {
      // exist: true  not exist: false
      if (data.includes(filepath)) rst = true;
    }).catch((err) => {
      throw new Error(err);
    });
    return rst;
  }

  /**
   * Generate the full path of the file.
   * @param {string} path file path
   * @param {string} fileName file name
   * @return {string} full path of the file
   */
  getFullPath(path = '', fileName) {
    logger.debug('path:' + path);
    logger.debug('fileName:' + fileName);

    if (objectStoreUtil.isEmptyStr(fileName)) {
      throw new Error('file name' + objectStoreUtil.ERR0001);
    }

    // Path shaping
    path = objectStoreUtil.trimChar(path.trim(), '/');

    // ファイルパス生成
    const filePath = objectStoreUtil.isEmptyStr(path) ?
    fileName : path + '/' + fileName;
    logger.debug('filePath:' + filePath);

    return filePath;
  }

  /**
   * get bucket name
   * @return {string} bucket name
   */
  getBucketName() {
    return this.#objectStoreInfo.bucket;
  }
};
