/* eslint-disable max-len */
// //--------------------------------------------------------------------------------------------------------------------*
// //Application Name : Check Functionality(Validation)
// // Object Id       : PJ-07
// // Release         : 1
// // Author          : CS
// // Date            : 17/11/2022
// // Description     : Common Validation check function
// //--------------------------------------------------------------------------------------------------------------------*
// //Descriptions: Validation check common processing class.
// //--------------------------------------------------------------------------------------------------------------------*
// //Change Log:
// //     Date                |   Author          |   Change Id     |   Change Description
// //--------------------------------------------------------------------------------------------------------------------*
// //recall log information
// //const commonLog = require('./lib/commonlogmessage');
// /**
//  * validate method Bean oData check common function
//  * @param oData         Entity object for uploaded oData
//    * @param oValidaters  Validation data check
//  * @param sLocale    sLocale object
//   * @return Message text and group information are returned as check results.
// */
// exports.validatecheck = function (oData,oValidaters,sLocale) {

//   //oData validation check from oSchema settings
//   oValidaters(oData);
//   //Validation check result confirmation
//   if (oValidaters.errors != null || oValidaters.errors != undefined ) {
//     const msgList = [];

//     //set english message
//     const oEnLog =  new commonLog(sLocale);
//     //Output validation check results one by one
//     oValidaters.errors.forEach(function(value) {
//       //Add to message list from property file from each check result
//       //Get fieldName
//       const fieldName = value.instancePath.substr(1);
//       switch (value.message) {
//         //Required input
//         case 'notEmptyId':
//           msgList.push(oEnLog.getLogMessage('ERR0001',[fieldName]));
//           break;
//         //When input is not possible
//         case 'reqEmptyId':
//           msgList.push(oEnLog.getLogMessage('ERR0002',[fieldName]));
//           break;
//         //String type check case
//         case 'chkCharId':
//           msgList.push(oEnLog.getLogMessage('ERR0004',[fieldName]));
//           break;
//         //If string length check
//         case 'chkLengthId':
//           msgList.push(oEnLog.getLogMessage('ERR0003',[fieldName]));
//           break;
//         //Number length check
//         case 'chkmumlengthId':
//           msgList.push(oEnLog.getLogMessage('ERR0003',[fieldName]));
//           break;
//         //If only numbers are checked
//         case 'isNumId':
//           msgList.push(oEnLog.getLogMessage('ERR0004',[fieldName]));
//           break;
//         //For numeric check
//         case 'isBigDecimalId':
//           msgList.push(oEnLog.getLogMessage('ERR0004',[fieldName]));
//           break;
//         //if date check
//         case 'isDateId':
//           msgList.push(oEnLog.getLogMessage('ERR0004',[fieldName]));
//           break;
//         //Permission/non-permission check
//         case 'chkChar':
//           msgList.push(oEnLog.getLogMessage('ERR0004',[fieldName]));
//           break;
//         default:
//           break;
//       }
//     });
//     // return messagelist
//     return msgList;
//   }
// }
