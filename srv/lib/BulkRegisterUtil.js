/* eslint-disable max-len */

// --------------------------------------------------------------------------------------------------------------------*
// Application Name : Mass upload excel file retrival function
// Object Id       : PJ-24
// Release         : 1
// Author          : songfeng.li
// Date            : 1/18/2022
// Description     : Mass upload excel file retrival function
// --------------------------------------------------------------------------------------------------------------------*
// Descriptions: Mass upload excel file retrival  class.
// --------------------------------------------------------------------------------------------------------------------*
// Change Log:
//     Date                |   Author          |   Change Id     |   Change Description
// --------------------------------------------------------------------------------------------------------------------*

const {createLogger} = require('@sap-cloud-sdk/util');
const ObjectStoreService = require('./ObjectStoreService');
const XLSX = require('xlsx');
const GX0170Constants = require('./GX0170constants');

module.exports = class BulkRegisterUtil {
  /** Logger declaration for log output */
  static #logger = createLogger('BulkRegisterUtil');

  /** object of ObjectStoreService */
  #objectStoreService = new ObjectStoreService();

  /**
   * excel file retrival function
   * @param {string} sInstanceName name of ObjectStore instance service
   * @param {string} sPath
   * @param {string} sfileName
   * @param {number} iSheetIndex
   * @return {Promise<number>}  WorkSheet of sheetIndex
   */
  async getExcelSheet(sInstanceName = GX0170Constants.PATH_DEFAULT, sPath,
      sfileName, iSheetIndex = 0) {
    if (sInstanceName == '') {
      sInstanceName = GX0170Constants.PATH_DEFAULT;
    }

    BulkRegisterUtil.#logger.debug('sheetIndex:' + iSheetIndex);
    BulkRegisterUtil.#logger.debug('sPath:' + sPath);
    BulkRegisterUtil.#logger.debug('sfileName:' + sfileName);
    BulkRegisterUtil.#logger.debug('sInstanceName:' + sInstanceName);
    // get file data from Object Store.
    const fileData = await this.#objectStoreService.getFile(sPath,
        sfileName, sInstanceName);

    // read as excel object
    const workbook = XLSX.read(fileData);
    // return the sheet of sheetIndex
    //  return
    const aoa = XLSX.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[iSheetIndex]], {header: 1});
    return aoa;
  }
};
