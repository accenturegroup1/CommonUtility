/* eslint-disable max-len */
const {parse} = require('csv-parse/sync');
const {createLogger} = require('@sap-cloud-sdk/util');
/**
 *
 * csv transform class
 * This function performs only data conversion processing and does not perform input check for each data.
 *
 * @author accenture
 * @version 01-00
 * @since 2022-12-09
 *
 */
module.exports = class ConvertCSV {
  // fixed value
  static VALUE_ZERO = '0';
  static LENGTH_ONE = 1;
  static LENGTH_THREE = 3;
  static LENGTH_FOUR = 4;
  static LENGTH_SIX = 6;
  static INDEX_ZERO = 0;
  static INDEX_TWO = 2;
  static INDEX_FOUR = 4;
  static INDEX_SIX = 6;
  static DEF_CRLF = '\r\n';
  static DEF_SPLITTYPE = ',';
  static DEF_ENCLOSINGCHARACTER = '"';
  static DEF_ESCAPE = '"';
  // logger
  static #logger = createLogger('utility-program');

  /**
   * Change string to line by line Jason array with the specified newline character.
   * The first row is the key, and the rows are converted to JSON format (key-value).
   * @param {string} sCRLF line break character default:\r\n
   * @param {string} sSplitType delimiter character default:,
   * @param {string} sEnclosingCharacter the characters used to surround a field default:"
   * @param {string} sFileData Data format conversion target
   * @return {Array<JSON>} succeed: sFileData's transformed data
   *                       failure: empty array []
   */
  static convertString2JSONArray(sCRLF, sSplitType, sEnclosingCharacter, sFileData) {
    ConvertCSV.#logger.debug('convertString2JSONArray -> start');
    ConvertCSV.#logger.debug('argment sCRLF -> ' + sCRLF);
    ConvertCSV.#logger.debug('argment sSplitType -> ' + sSplitType);
    ConvertCSV.#logger.debug('argment sEnclosingCharacter -> ' + sEnclosingCharacter);
    ConvertCSV.#logger.debug('argment sFileData -> ' + sFileData);

    // line break (\r\n or \r or \n) default:\r\n
    if (!ConvertCSV.#isNotEmptyString(sCRLF)) {
      sCRLF = ConvertCSV.DEF_CRLF;
    }
    // delimiter default:,
    if (!ConvertCSV.#isNotEmptyString(sSplitType)) {
      sSplitType = ConvertCSV.DEF_SPLITTYPE;
    }
    // the characters used to surround a field default:"
    if (!ConvertCSV.#isNotEmptyString(sEnclosingCharacter)) {
      sEnclosingCharacter = ConvertCSV.DEF_ENCLOSINGCHARACTER;
    }

    if (sCRLF != '\r\n' && sCRLF != '\r' && sCRLF != '\n') {
      throw new RangeError('sCRLF: The value of parameter is incorrect.');
    }

    let fileDataResult = [];

    // if sFileData is not empty
    if (ConvertCSV.#isNotEmptyString(sFileData)) {
      try {
        fileDataResult = parse(sFileData, {
          record_delimiter: sCRLF,
          delimiter: sSplitType,
          quote: sEnclosingCharacter,
          escape: ConvertCSV.DEF_ESCAPE,
          skip_empty_lines: true,
          columns: true,
        });
      } catch (error) {
        ConvertCSV.#logger.error(error);
      }
    }

    ConvertCSV.#logger.debug('fileDataResult -> ' + fileDataResult);
    ConvertCSV.#logger.debug('convertString2JSONArray -> end');
    return fileDataResult;
  }

  /**
   * Date string converted to YYYYMMDD format.
   * @param {string} sData data format conversion target
   * @return {string} succeed: sData's transformed data
   *                  failure: sData
   */
  static convertString2Date(sData) {
    ConvertCSV.#logger.debug('convertString2Date -> start');
    ConvertCSV.#logger.debug('argment sData -> ' + sData);

    // the parameter is not empty
    if (ConvertCSV.#isNotEmptyString(sData)) {
      // divide the parameters by /
      const rData = sData.replaceAll(' ', '').split('/');

      // when the number of elements is 3
      if (rData.length === ConvertCSV.LENGTH_THREE) {
        // element1:4 digits && element2:2 digits && element3:1 digit or 2 digits
        if (rData[0].length === ConvertCSV.LENGTH_FOUR &&
                    rData[1].length < ConvertCSV.LENGTH_THREE &&
                    rData[2].length < ConvertCSV.LENGTH_THREE) {
          // when element2 has one digit number
          if (rData[1].length === ConvertCSV.LENGTH_ONE) {
            // add a zero to the top
            rData[1] = ConvertCSV.VALUE_ZERO + rData[1];
          }

          // when element3 has one digit number
          if (rData[2].length === ConvertCSV.LENGTH_ONE) {
            // add a zero to the top
            rData[2] = ConvertCSV.VALUE_ZERO + rData[2];
          }

          // sData = element1 + element2 + element3
          sData = rData[0] + rData[1] + rData[2];
        }
      }
    }

    ConvertCSV.#logger.debug('sData -> ' + sData);
    ConvertCSV.#logger.debug('convertString2Date -> end');

    return sData;
  }

  /**
   * Time type string converted to HH:MM:SS format.
   * @param {string} sTime time format conversion target
   * @return {string} succeed: sTime's transformed data
   *                  failure: sTime
   */
  static convertString2Time(sTime) {
    ConvertCSV.#logger.debug('convertString2Time -> start');
    ConvertCSV.#logger.debug('argment sTime -> ' + sTime);

    // the parameter is not empty
    if (ConvertCSV.#isNotEmptyString(sTime)) {
      // divide the parameters by :
      const rTime = sTime.replaceAll(' ', '').split(':');

      // when the number of elements is 1
      if (rTime.length == ConvertCSV.LENGTH_ONE) {
        // when the parameter has 6 digits
        if (sTime.length == ConvertCSV.LENGTH_SIX) {
          // time type string converted to HH:MM:SS format
          sTime = sTime.substring(ConvertCSV.INDEX_ZERO, ConvertCSV.INDEX_TWO) + ':' +
                        sTime.substring(ConvertCSV.INDEX_TWO, ConvertCSV.INDEX_FOUR) + ':' +
                        sTime.substring(ConvertCSV.INDEX_FOUR, ConvertCSV.INDEX_SIX);
        }

        // when the number of elements is 3
      } else if (rTime.length == ConvertCSV.LENGTH_THREE) {
        // when the number of digits of element1 is 1 digit, add a zero to the top
        if (rTime[0].length == ConvertCSV.LENGTH_ONE) {
          rTime[0] = ConvertCSV.VALUE_ZERO + rTime[0];
        }

        // sTime ＝ element1 ＋”:”＋ element2 ＋”:”＋ element3
        sTime = rTime[0] + ':' + rTime[1] + ':' + rTime[2];
      }
    }

    ConvertCSV.#logger.debug('sTime -> ' + sTime);
    ConvertCSV.#logger.debug('convertString2Time -> end');

    return sTime;
  }

  /**
   * Returns the string that ',' is removed from the numeric string of the parameter.
   * @param {string} sNum number format conversion target
   * @return {string} succeed: sNum's transformed data
   *                  failure: sNum
   */
  static convertString2Numeric(sNum) {
    ConvertCSV.#logger.debug('convertString2Numeric -> start');
    ConvertCSV.#logger.debug('argment sNum -> ' + sNum);

    // the parameter is not empty
    if (ConvertCSV.#isNotEmptyString(sNum)) {
      sNum = sNum.replaceAll(' ', '').replaceAll(',', '');
    }

    ConvertCSV.#logger.debug('sNum -> ' + sNum);
    ConvertCSV.#logger.debug('convertString2Numeric -> end');

    return sNum;
  }

  /**
   * Checking not empty string
   * @param {string} val check target
   * @return {boolean} not empty string: true
   *                               else: false
   */
  static #isNotEmptyString(val) {
    if (typeof val == 'string' && val.length > 0) {
      return true;
    }
    return false;
  }
};
