// ----------------------------------------------------------------------------*
// Application Name : BTP Object Store common function(To Store file data)
// Object Id       : PJ-21
// Release         : 1
// Author          : xianyu.pan
// Date            : 23/12/2022
// Description     : upload or download file by BTP Object Store
// ----------------------------------------------------------------------------*
// Descriptions: Entity base class.
// ----------------------------------------------------------------------------*
// Change Log:
//     Date          |   Author       |   Change Id     |   Change Description
// ----------------------------------------------------------------------------*

/**
 * Entity base class
 */
const {createLogger} = require('@sap-cloud-sdk/util');
const logger = createLogger('utility-program');
const objectStoreUtil = require('./ObjectStoreUtil');
const AwsS3 = require('./AwsS3Service');
const xsenv = require('@sap/xsenv');

module.exports = class ObjectStoreService {
  #services; // all servers

  /**
   * constructor
   */
  constructor() {
    this.#services = xsenv.readServices();
  }

  /**
     * Save the file to the ObjectStore.
     * @param {BinaryType} data Binary data
     * @param {string} path file path
     * @param {string} fileName file name
     * @param {string} instanceName object store intance name
     */
  saveFile(data, path = '', fileName, instanceName = null) {
    try {
      const storage = this.#doCommonProcess(instanceName);
      storage.saveFile(data, path, fileName);
    } catch (err) {
      logger.error(err);
      throw new Error(err);
    }
  }

  /**
     * Get files in the ObjectStore.
     * @param {string} path  file path
     * @param {string} fileName  file name
     * @param {string} instanceName object store intance name
     * @return {BinaryType} Binary of file
     */
  async getFile(path = '', fileName, instanceName = null) {
    let file = null;
    try {
      const storage = this.#doCommonProcess(instanceName);
      file = await storage.getFile(path, fileName);
    } catch (err) {
      logger.error(err);
      throw new Error(err);
    }
    return file;
  }

  /**
     * Delete files in the ObjectStore.
     * @param {string} path file path
     * @param {string} fileName file name
     * @param {string} instanceName object store intance name
     */
  deleteFile(path = '', fileName, instanceName = null) {
    try {
      const storage = this.#doCommonProcess(instanceName);
      storage.deleteFile(path, fileName);
    } catch (err) {
      logger.error(err);
      throw new Error(err);
    }
  }

  /**
     * Get the ObjectStore bucket
     * @param {string} instanceName object store intance name
     * @return {string} BucketName
     */
  getBucketName(instanceName = null) {
    let bucketName = '';
    try {
      const storage = this.#doCommonProcess(instanceName);
      bucketName = storage.getBucketName();
    } catch (err) {
      logger.error(err);
      throw new Error(err);
    }
    return bucketName;
  }

  /**
    * Check if the ObjectStore file exists.
    * @param {string} path file path
    * @param {string} fileName file name
    * @param {string} instanceName object store intance name
    * @return {boolean} exist: true
    *                   not exist: false
    */
  async isFileExist(path = '', fileName, instanceName = null) {
    let isExist = false;
    try {
      const storage = this.#doCommonProcess(instanceName);
      // File path generation
      const filePath = storage.getFullPath(path, fileName);
      // ファイル取得処理
      isExist = await storage.isFileExist(filePath);
    } catch (err) {
      logger.error(err);
      throw new Error(err);
    }
    return isExist;
  }

  /**
   * Get Services of storage
   * @param {string} instanceName The instance name of the ObjectStore to access
   * @return {object} storage
   */
  #doCommonProcess(instanceName) {
    try {
      let storage = null;

      // Setting Initial Values
      if (objectStoreUtil.isEmptyStr(instanceName)) {
        instanceName = objectStoreUtil.INSTANCE_NAME_DEFAULT;
      }
      logger.info('instancename: ' + instanceName);
      // Extract instance information from instanceName
      const osServiceName =
          Object.keys(this.#services).
              filter((key) => key.endsWith(instanceName))[0];
      const info = this.#services[osServiceName];
      if (info) {
        const objectStroeInfo = {
          name: info.name,
          instance_name: info.instance_name,
          plan: info.plan,
          bucket: info.credentials.bucket,
          access_key_id: info.credentials.access_key_id,
          secret_access_key: info.credentials.secret_access_key,
          host: info.credentials.host,
          region: info.credentials.region,
          source: info.credentials.source,
        };
        // Get Services of storage by Service Type
        if (objectStroeInfo.source === objectStoreUtil.serviceType.PLAN_AWS) {
          storage = new AwsS3(objectStroeInfo);
        } else {
          storage = new AwsS3(objectStroeInfo);
        }
      }
      return storage;
    } catch (err) {
      logger.error(objectStoreUtil.ERR0002 + ' ' + err);
      throw new Error(objectStoreUtil.ERR0002 + ' ' + err);
    }
  }
};
