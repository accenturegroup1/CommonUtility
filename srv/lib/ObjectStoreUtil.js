module.exports = class Constants {
  static INSTANCE_NAME_DEFAULT = 'store-gx';

  static serviceType = {
    PROVIDER_AWS: 'aws-s3',
    PROVIDER_GCP: 'google-cloud-storage',
    PROVIDER_AZURE: 'azureblob',
    PLAN_AWS: 'S3',
  };

  static ERR0001 = ' is null or empty.';
  static ERR0002 = 'Error while getting ObjectStore credentials.';
  static ERR0003 = ' already exists in the container.';
  static ERR0004 = ' does not exist in the container.';
  static ERR0005 = 'Error occured while uploading the object: ';
  static ERR0006 = 'Error occured while downloading the object: ';
  static ERR0007 = 'Error occured while deleting the object: ';
  static INF0001 = ' is successfully uploaded.';
  static INF0002 = ' is successfully downloaded.';
  static INF0003 = ' is successfully deleted.';

  /**
     * is empty string
     * @param {string} s
     * @return {boolean}
     */
  static isEmptyStr(s) {
    if (s == null || s === '') {
      return true;
    }
    return false;
  }

  /**
     * is not empty string
     * @param {string} val
     * @return {boolean}
     */
  static isNotEmptyStr(val) {
    if (typeof val == 'string' && val.length > 0) {
      return true;
    }
    return false;
  }

  /**
     * trimChar
     * @param {string} str
     * @param {string} char
     * @return {string}
     */
  static trimChar(str, char) {
    if (char) {
      str = str.replace(new RegExp('^\\' + char +
                                     '+|\\' + char + '+$', 'g'), '');
    }
    return str;
  }
};

