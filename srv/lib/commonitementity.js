/* eslint-disable max-len */


// --------------------------------------------------------------------------------------------------------------------*
// Application Name : Add-on table common fields update function
// Object Id       : PJ-24
// Release         : 1
// Author          : songfeng.li
// Date            : 1/18/2023
// Description     : Add-on table common fields update function
// --------------------------------------------------------------------------------------------------------------------*
// Descriptions: Entity base class.
// --------------------------------------------------------------------------------------------------------------------*
// Change Log:
//     Date                |   Author          |   Change Id     |   Change Description
// --------------------------------------------------------------------------------------------------------------------*
/**
 * Entity base class
 */
module.exports = class CommonItemEntity {
  /**
    * constructor
    */
  constructor() {
    /** Registered Date */
    this.regdate = null;
    /** Registered User */
    this.reguser = '';
    /** Registered Program */
    this.regpgm = '';
    /** Updated  Date */
    this.upddate = null;
    /** Updated User */
    this.upduser = '';
    /** Updated Program */
    this.updpgm = '';
  }
};

