/* eslint-disable max-len */

// --------------------------------------------------------------------------------------------------------------------*
// Application Name : Add-on table common fields update function
// Object Id       : PJ-24
// Release         : 1
// Author          : songfeng.li
// Date            : 1/18/2023
// Description     : Add-on table common fields update function
// --------------------------------------------------------------------------------------------------------------------*
// Descriptions: Entity base class.
// --------------------------------------------------------------------------------------------------------------------*
// Change Log:
//     Date                |   Author          |   Change Id     |   Change Description
// --------------------------------------------------------------------------------------------------------------------*
const {createLogger} = require('@sap-cloud-sdk/util');

/**
 * table common fields update class
 */
module.exports = class CommonItemSet {
  /** Logger declaration for log output */
  static #logger = createLogger('CommonItemSet');
  /** Date and time key */
  static #KYEREGDATE = 'keyRegdate';
  /** user key */
  static #KYEREGUSER = 'keyReguser';
  /**  cache for user and date   */
  #mConstantMap = new Map();
  /** Date and time */
  #regdate = null;
  /** user */
  #sReguser = '';

  /**
   *setting common fields of tables function.
   *
   * @param {CommonItemEntity} oCommonItemEntity  entity
   * @param {string} sPGMID progameID
   * @param {string} sUser login user *email
   */
  setItem(oCommonItemEntity, sPGMID, sUser) {
    // user and date has cached
    if (this.#mConstantMap.has(CommonItemSet.#KYEREGDATE) &&this.#mConstantMap.has(CommonItemSet.#KYEREGUSER)) {
      // ftech cached user and date.
      // date
      this.#regdate = this.#mConstantMap.get(CommonItemSet.#KYEREGDATE);

      // user
      this.#sReguser = this.#mConstantMap.get(CommonItemSet.#KYEREGUSER);

      CommonItemSet.#logger.info('Map Value Get.');
    } else {
      // cache user(login user) and date(system date).
      // date
      this.#mConstantMap.set(CommonItemSet.#KYEREGDATE, new Date(Date.now()));
      this.#regdate = this.#mConstantMap.get(CommonItemSet.#KYEREGDATE);
      CommonItemSet.#logger.info('sRegdate:' + this.#mConstantMap.get(CommonItemSet.#KYEREGDATE));
      CommonItemSet.#logger.info('sReguser:' + sUser);
      // email of login user
      this.#mConstantMap.set(CommonItemSet.#KYEREGUSER, sUser);
      this.#sReguser = this.#mConstantMap.get(CommonItemSet.#KYEREGUSER);

      CommonItemSet.#logger.info('New Date Value.');
    }

    // condition of registered
    if (oCommonItemEntity.regdate === null && oCommonItemEntity.reguser === '' && oCommonItemEntity.regpgm === '') {
      // Registered date
      oCommonItemEntity.regdate = this.#regdate;
      // Registered user
      oCommonItemEntity.reguser = this.#sReguser;
      // Registered program
      oCommonItemEntity.regpgm = sPGMID;
    }
    // Updated  date
    oCommonItemEntity.upddate = this.#regdate;
    // Updated  user
    oCommonItemEntity.upduser = this.#sReguser;
    // Updated  program
    oCommonItemEntity.updpgm = sPGMID;
  }
};
