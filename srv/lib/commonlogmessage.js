/* eslint-disable linebreak-style */
/* eslint-disable max-len */
// --------------------------------------------------------------------------------------------------------------------*
// Application Name : Common Functions Module
// Object Id       : PJ-03
// Release         : 1
// Author          : Mahesh Magar
// Date            : 17/11/2022
// Description     : Common Log Message acquisition function
// --------------------------------------------------------------------------------------------------------------------*
// Descriptions: Function implementation done for Common Log Message acquisition functionality.
// --------------------------------------------------------------------------------------------------------------------*
// Change Log:
//     Date                |   Author          |   Change Id     |   Change Description
// --------------------------------------------------------------------------------------------------------------------*
const path = require('path');
/** Using sap textbundle  */
const TextBundle = require('@sap/textbundle').TextBundle;
/* text bundle used for local messages */
const messageBundle = new TextBundle('./_i18n/i18n');
const config = require('./config/constants');
/**
 * If locale is not supported or maintained then text is fetched  default from i18n.properties file
 * @classdesc Load Message bundles based on locale
 * @param {string} [sLocale] Bundle locale (use of standard BCP 47 locales is recommended, POSIX is supported), default to 'en'.
 * @property {string} locale TestBundle main locale
 * @constructor
 */
function CommonLog(sLocale) {
  const propertyFilePath = config.globalMessages.filePath;

  if (!propertyFilePath || typeof propertyFilePath !== 'string') {
    throw new Error(messageBundle.getText('msgInvalidBaseFile'));
  }
  // _locales uses POSIX locales whereas _locale could be either POSIX or BCP 47
  this._locale = sLocale || 'en';
  // Fetch textBundle based on locale
  this._bundle = new TextBundle(path.resolve(__dirname, propertyFilePath), sLocale);
}
/**
 * Returns a formatted message for the given resource key and arguments
 * getLogMessage method Outputs the processing results of processing functions executed on BTP to the log on BTP.
 * Based on the same concept as SAP UI5, this module works with UTF-8 encoded properties files. Language defaulting is also borrowed from SAP UI5 with the idea the UI and server-side code use the same text internationalization approach.
 * @param {string} sMessageId Resoruce key
 * @param {Array} [aParams] array of params
 * @return {string} Message text
 */
CommonLog.prototype.getLogMessage = function(sMessageId, aParams) {
  let messageText = null;

  if (sMessageId) {
    // check of any arguments are passed
    if (!aParams) {
      messageText = this._bundle.getText(sMessageId);
    }

    // if any arguments are passed develope message with params
    if (aParams && aParams.length > 0) {
      messageText = this._bundle.getText(sMessageId, aParams);
    }

    if (typeof messageText === 'string' && messageText !== sMessageId) {
      return messageText;
    } else {
      throw new Error(messageBundle.getText('errMsgNotFoud', [sMessageId]));
    }
  } else {
    throw new Error(messageBundle.getText('errMissingMsgId'));
  }
};

module.exports = CommonLog;

