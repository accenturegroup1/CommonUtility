/* eslint-disable linebreak-style */
/* eslint-disable max-len */

// --------------------------------------------------------------------------------------------------------------------*
// Application Name : Common Functions Module
// Object Id       : PJ-02
// Release         : 1
// Author          : Mahesh Magar
// Date            : 17/11/2022
// Description     : Common Log Ouput
// --------------------------------------------------------------------------------------------------------------------*
// Descriptions: Function implementation to provide common output after Application processing.
// --------------------------------------------------------------------------------------------------------------------*
// Change Log:
//     Date                |   Author          |   Change Id     |   Change Description
// --------------------------------------------------------------------------------------------------------------------*
/** Using sap textbundle  */
const TextBundle = require('@sap/textbundle').TextBundle;
/* text bundle used for local messages */
const oMessageBundle = new TextBundle('./_i18n/i18n');
/* Logger for cf for application logging*/
const log = require('cf-nodejs-logging-support');

/**
 * outputLog method Outputs the processing results of processing functions executed on BTP to the log on BTP.
 *
 * @param {Integer}iAllCount       Total number of cases processed
 * @param {Integer}iSuccesCount   number of successes
 * @param {Integer}iErrorCount     number of error
 * @param {Integer}iSkipCount      number of skip
 * @param {Integer}isExcludOutput Excluded number output flag(True:output, False:not output)
 * @param {Integer}iExcludCount  Excluded cases
 * @param {Ineger}iProcessResult result(0:successes,1:warning,2:error)
 * @return {String} processed output message
 */
exports.outputLog = function(iAllCount, iSuccesCount, iErrorCount, iSkipCount,
    isExcludOutput, iExcludCount, iProcessResult) {
  let outputMessage = '';

  /**
     * Adds a message according to the processing result of the argument.
     * If the processing result is unexpected, terminate this processing.
     */
  switch (iProcessResult) {
    case 0:
      outputMessage = oMessageBundle.getText('msgSuccess');
      break;
    case 1:
      outputMessage = oMessageBundle.getText('msgError');
      break;
    case 2:
      outputMessage = oMessageBundle.getText('msgWarning');
      break;
    default:
      log.error(oMessageBundle.getText('errInvalidStatus'));
      return;
  }
  // create message based on iExcludeFlag
  outputMessage = isExcludOutput ? `${outputMessage}${oMessageBundle.getText('msgCommonLogOutput', [iAllCount, iSuccesCount, iErrorCount, iSkipCount, iExcludCount])}` :
                                   `${outputMessage}${oMessageBundle.getText('msgCommonLogOutputWoExclude', [iAllCount, iSuccesCount, iErrorCount, iSkipCount])}`;

  log.info(outputMessage);
  // Output the created message to the log.

  return outputMessage;
// eslint-disable-next-line linebreak-style
};
