/* eslint-disable brace-style */
/* eslint-disable valid-jsdoc */
/* eslint-disable max-len */
/* eslint-disable no-trailing-spaces */
/**
 * Date conversion function(Local / UTC date and time acquisition function etc.)
 * 
 * @author C.S
 * @version 1.0
 * @see ChangeUtcLocalDate Common date conversion function
 * @since 2022-12-06
 */
// import library
require('date-utils');
const momentTimezone = require('moment-timezone');
const createLogger = require('@sap-cloud-sdk/util').createLogger;
const LOGGER = createLogger('utility-program');
const TextBundle = require('@sap/textbundle').TextBundle;
const configpro = new TextBundle('./_i18n/conf');

// eslint-disable-next-line valid-jsdoc
/**
 * changeUtcToLocal method
 * Convert the standard system date and time (UTC) set on the BTP server to the local date and time set on S/4HANA
 * 
 * @param sUtcDateTime      Date Time（UTC）
 * @param sOutputDateFormat Date conversion format (local)
 * @param sDestination      KEY value specified in the method
 // eslint-disable-next-line max-len
 * @return {@code if successful} sOutputDate date after conversion {@code if unsuccessful} null
 */
exports.changeUtcToLocal = function(sUtcDateTime, sOutputDateFormat, sDestination) {
  // 1 month minus(month starts from 0)
  sUtcDateTime.setMonth(sUtcDateTime.getMonth() - 1);
  LOGGER.debug('changeUtcToLocal -> start');
  LOGGER.debug('argment sUtcDateTime -> ' + sUtcDateTime);
  LOGGER.debug('argment sOutputDateFormat -> ' + sOutputDateFormat);
  LOGGER.debug('argment sDestination -> ' + sDestination);

  // Generate format class using input parameters
  LOGGER.debug('constantConfig -> start');
  const constant = configpro.getText(configpro.getText(sDestination));
  // If the acquired data itself is null or the Key set in sDestination does not exist
  if (constant === null) {
    LOGGER.error('Failed to get the Local DateTime');
    return null;
  }
  try {
    // Generate UTC moment object
    const dateTimeUtc = momentTimezone.tz(sUtcDateTime, 'UTC');
    // Convert to JST (convert the format as well)
    sOutputDate = momentTimezone(dateTimeUtc).tz(constant).format(sOutputDateFormat);
    LOGGER.debug('sOutputDate ->' + sOutputDate);
    LOGGER.debug('changeUtcToLocal -> end');
    // If normal, return date after conversion
    return sOutputDate;
  }
  // If the value after conversion is null or date conversion fails
  catch (error) {
    LOGGER.error('Failed to get the Local DateTime');

    // return null to caller
    return null;
  }
};

/**
 * changeLocalToUtc method
 * changeLocalToUtc method Convert local date and time set in S/4HANA to standard system date and time (UTC)
 * 
 * @param sLocalDateTime    Date Time (local)
 * @param sOutputDateFormat Date conversion format (UTC)
 * @return {@code if successful} sLocalDateTimeSetUTC date after conversion {@code if unsuccessful} null
 */
exports.changeLocalToUtc = function(sLocalDateTime, sOutputDateFormat) {
  // 1 month minus(month starts from 0)
  sLocalDateTime.setMonth(sLocalDateTime.getMonth() - 1);
  LOGGER.debug('changeLocalToUtc -> start');
  LOGGER.debug('argment sLocalDateTime -> ' + sLocalDateTime);
  LOGGER.debug('argment sOutputDateFormat -> ' + sOutputDateFormat);

  // Set the retrieved UTC timezone
  try {
    // Generate UTC moment object
    const dateTimeUtc = momentTimezone.utc(sLocalDateTime, 'UTC');
    // Convert to UTC (convert the format as well)
    sLocalDateTimeSetUTC = momentTimezone(dateTimeUtc).format(sOutputDateFormat);

    // If normal, return date after conversion
    return sLocalDateTimeSetUTC;
  }
  // When sLocalDateTimeSetUTC is an invalid parameter
  catch (error) {
    LOGGER.error('Failed to get the UTC DateTime');
    // return null to caller
    return null;
  }
};
