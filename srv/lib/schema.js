/* eslint-disable max-len */
// const Ajv = require("ajv");
// //ajv information format setting
// const addFormats = require("ajv-formats");
// //create ajv
// const ajv = new Ajv({ allErrors: true });
// //const beancheckutil = require ('./lib/BeanCheckUtil');

// ajv.addFormat('numlencheck', {
//   type: "number",
//   validate: (stampNumber) => {
//     //Integer length value
//     const stampNum = stampNumber.toString().split('.')[0].length;
//     //fraction length value
//     const stampNumberDec = stampNumber.toString().split('.')[1].length;
//     // let integerDigit = parseInt(inputNumber).toString().length;
//     if (stampNumberDec > 0){
//       return (stampNum > 0 && stampNum < 7 && stampNumberDec > 0 && stampNumberDec < 3)
//     }else{
//       return (stampNum > 0 && stampNum < 7 )
//     }
//   }
// })
// ajv.addFormat('intlencheck', {
//   type: "number",
//   validate: (inputInt) => {
//     const integerDigit = parseInt(inputInt).toString().length;
//     return (integerDigit > 0 && integerDigit < 5 )
//   }
// })
// //self-defined message definition
// require("ajv-errors")(ajv);
// addFormats(ajv);
// //Setting shema rules
// exports.schema = function () {
//   const schemavaules = {
//     type: 'object',
//     properties: {
//         //Required input check
//         name: {
//           type: "string",
//           minLength: 1,
//           errorMessage: {
//             type: "chkCharId",
//             minLength: "notEmptyId",
//           },
//         },
//         //Input disabled check
//         nullcheck: {
//           type:"string",
//           maxLength: 0 ,
//           errorMessage: {
//             type: "chkCharId",
//             maxLength: "reqEmptyId",
//           },
//         },
//         //length check
//         id: {
//           type: "string" ,
//           minLength: 3,
//           maxLength: 5 ,
//           errorMessage: {
//             type: "chkCharId",
//             minLength: "chkLengthId",
//             maxLength: "chkLengthId",
//           },
//         },
//         // integer and length check
//         age: {
//           type: "integer",
//           format: 'intlencheck',
//           errorMessage: {
//             type: "isNumId",
//             format: "chkmumlengthId",
//             },
//         },
//         //Number and length check
//         money: {
//           type: 'number',
//           format: 'numlencheck',
//           errorMessage: {
//             type: "isBigDecimalId",
//             format: "chkmumlengthId",
//             },
//         },
//         //Date check
//         todays: {
//           type: "string",
//           format: "date",
//           errorMessage: {
//             type: "chkCharId",
//             format: "isDateId",
//             },
//         },
//         //Input enabled/disabled check
//         rule: {
//           type: "string" ,
//           pattern: '[abc]+',
//           errorMessage: {
//             type: "chkCharId",
//             pattern: "chkChar",
//             },
//         },
//         //Required input check
//         nameng: {
//           type: "string",
//           minLength: 1,
//           errorMessage: {
//             type: "chkCharId",
//             minLength: "notEmptyId",
//           },
//         },
//         //Input disabled check
//         nullcheckng: {
//           type:"string",
//           maxLength: 0 ,
//           errorMessage: {
//             type: "chkCharId",
//             maxLength: "reqEmptyId",
//           },
//         },
//         //Number and length check
//         moneyng: {
//           type: 'number',
//           format: 'numlencheck',
//           errorMessage: {
//             type: "isBigDecimalId",
//             format: "chkmumlengthId",
//             },
//         },
//         //Required input check
//         notEmptyIdByOK: {
//           type: "string",
//           minLength: 1,
//           errorMessage: {
//             type: "chkCharId",
//             minLength: "notEmptyId",
//           },
//         },
//         //Input disabled check
//         reqEmptyIdByOK: {
//           type:"string",
//           maxLength: 0 ,
//           errorMessage: {
//             type: "chkCharId",
//             maxLength: "reqEmptyId",
//           },
//         },
//         //length check
//         chkCharIdByOK: {
//           type: "string" ,
//           minLength: 1,
//           maxLength: 12 ,
//           errorMessage: {
//             type: "chkCharId",
//             minLength: "chkLengthId",
//             maxLength: "chkLengthId",
//           },
//         },
//         //Number and length check
//         chkmumlengthIdByOK: {
//           type: "integer",
//           format: 'intlencheck',
//           errorMessage: {
//             type: "isNumId",
//             format: "chkmumlengthId",
//             },
//         },
//         //Number and length check
//         isBigDecimalIdByOK: {
//           type: 'number',
//           format: 'numlencheck',
//           errorMessage: {
//             type: "isBigDecimalId",
//             format: "chkmumlengthId",
//             },
//         },
//         //Date check
//         isDateIdByOK: {
//           type: "string",
//           format: "date",
//           errorMessage: {
//             type: "chkCharId",
//             format: "isDateId",
//             },
//         },
//         //Input enabled/disabled check
//         chkCharByOK: {
//           type: "string" ,
//           pattern: '[abc]+',
//           errorMessage: {
//             type: "chkCharId",
//             pattern: "chkChar",
//             },
//         },
//     },
//     //Mandatory input setting
//     required: [],
//     //If true, allow parameters other than name
//     additionalProperties: false,

//   };


//   //oData validation check from oSchema settings
//   const oValidaters = ajv.compile(schemavaules);
//   // return validation
//   return oValidaters
// }
