/* eslint-disable max-len */
// -----------------------------------------------------------------------------------*
// Application Name :   Common Utility Function
// Object Id        :   PJ-17
// Release          :
// Author           :   Mahesh Magar
// Date             :   12/12/2022
// Description      :   Job Execution Variable Managament
// -----------------------------------------------------------------------------------*
// Descriptions: module to handle Hana cloud database operations
// -----------------------------------------------------------------------------------*
// Change Log:
//    Date      |   Author      |   Defect/Incident     |   Change Description
//
// -----------------------------------------------------------------------------------*/

const cds = require('@sap/cds');
const log = require('cf-nodejs-logging-support');

const {ExecVariableDefinition, ExecVariableRetrival} = cds.entities;

/**
 * Function to get Data from Execution Variable definition Table
 * @param {String} sJobId Job Id maintained in Table
 * @return {Array} return array of fetched data if corresponding data is found.
 */
async function getExecVarFromDefinition(sJobId) {
  log.info(`get Execution variable  from definition table`);

  const executionVariableData =
        await cds.run(SELECT.from(ExecVariableDefinition)
            .columns(['jobId',
              'varName',
              'seqNo',
              'varType',
              'dynVarId',
              'dataType',
              'sign',
              'lowValue',
              'highValue'])
            .where({
              jobId: sJobId,
            }));

  return executionVariableData;
}


/**
 * function to fetch execution variables from Retrieval table
 * @param {String} sJobId Job Id maintained in Table
 * @return {Array} return array of fetched data if corresponding data is found.
 */
async function getExecVarFromRetrival(sJobId) {
  log.info(`get Execution variable from Retrieval table`);

  const executionVariableData = await cds.run(SELECT.from(ExecVariableRetrival)
      .columns(['jobId',
        'varName',
        'seqNo',
        'varType',
        'dynVarId',
        'dataType',
        'sign',
        'lowValue',
        'highValue'])
      .where({
        jobId: sJobId,
      }));
  return executionVariableData;
}

/**
 * function to delete execution variables data from  retrieval table
 * @param {String} sJobId Job Id maintained in Table
 * @return {Number} number of deleted records
 */
async function deleteExecVarFromRetrival(sJobId) {
  log.info(`delete Execution variable data from Retrieval table`);

  const deleteExecutionVariableDataCount = await cds.run(DELETE.from(ExecVariableRetrival)
      .where({
        jobId: sJobId,
      }));

  return deleteExecutionVariableDataCount;
}

/**
 * function to delete execution variables data from  retrieval table
 * @param {Array} aData array of entries to be created
 * @return {*} created records object
 */
async function createExecVarDetails(aData) {
  log.info(`create Entry in  Execution variable Retrieval  table`);

  const executionVariableData = await cds.run(INSERT.into(ExecVariableRetrival, aData));
  return executionVariableData;
}

module.exports = {
  getExecVarFromDefinition, getExecVarFromRetrival, deleteExecVarFromRetrival, createExecVarDetails,
};
