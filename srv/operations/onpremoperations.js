/* eslint-disable max-len */
// -----------------------------------------------------------------------------------*
// Application Name :   Common Utility Function
// Object Id        :   PJ-17
// Release          :
// Author           :   Mahesh Magar
// Date             :   12/12/2022
// Description      :   Job Execution Variable Managament
// -----------------------------------------------------------------------------------*
// Descriptions: module to handle s4 cloud database operations
// -----------------------------------------------------------------------------------*
// Change Log:
//    Date      |   Author      |   Defect/Incident     |   Change Description
//
// -----------------------------------------------------------------------------------*/

const cds = require('@sap/cds');
const log = require('cf-nodejs-logging-support');
const constants = require('../config/constant');
const CommonLog = require('../lib/commonlogmessage');

// function to fetch execution variables from Definition table
/**
 * Fetch Dynamic variable from s/4 table
 * @param {*} req request context
 * @param {*} sDynVarId Dynamic Variable Id stored in S/4 TVARVC table
 * @return {Array} Dynamic variable fetched from backend
 */
async function getDynamicVariable(req, sDynVarId) {
  const commonLog = new CommonLog(req.locale);

  log.info(`get Dynamic Variable from S4 table`);
  // Using CDS API
  const serviceName = await cds.connect.to(constants.serviceName);
  let dynVariable = await serviceName.
      run(SELECT.from('TvarcEntityCollection').where({
        Name: sDynVarId,
      }));
  if (dynVariable && Array.isArray(dynVariable)) {
    dynVariable = dynVariable.filter((dynVar) => {
      return (dynVar.Name === sDynVarId);
    });
    if (dynVariable.length == 0) {
      log.info(`No value maintained for variable ${sDynVarId}`);
      req.error({code: constants.httpError400,
        message: commonLog.
            getLogMessage('errConfigurationMissing', [sDynVarId])});
    }
  }
  return dynVariable;
}


module.exports = {
  getDynamicVariable,
};
